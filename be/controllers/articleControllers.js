const Article = require('../models/Article');

module.exports.createArticle = (req,res) => {

	let newArticle = new Article({

				title : req.body.title,
				content : req.body.content
			})

			newArticle.save()			
			return res.send(newArticle)
}


module.exports.getAllArticle = (req,res) => {

	Article.find({})
	.then(articles => res.send(articles))
	.catch(error => res.send(error))
}

module.exports.getSingleArticle = (req,res) => {

	//console.log(req.params.id)

	Article.findById(req.params.id)
	.then(article => res.send(article))
	.catch(error => res.send(error))

}

module.exports.updateArticle = (req,res) => {

	let updates = {
		title: req.body.title,
		content: req.body.content
	}

	console.log(req.body)

	Article.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.deleteArticle = (req,res) => {

	console.log(req.params.id)
	Article.findByIdAndDelete(req.params.id)
	.then(result => res.send("Delete Successful"))
	.catch(error => res.send(error))
}