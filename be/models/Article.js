const mongoose = require('mongoose');

const articleSchema = new mongoose.Schema({

	title: {
		type: String,
		required: [true, "Title is required."]
	},
	content:{
		type: String,
		required: [true, "Content is required."]
	},
	dateCreated:{
		type: Date,
		default: new Date()
	}

})

module.exports = mongoose.model('Article', articleSchema);