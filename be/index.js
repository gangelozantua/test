const express = require('express');
const app = express();
const mongoose = require('mongoose');

const port = 4000;


mongoose.connect('mongodb+srv://angzang:angelo@cluster0.j7r8u.mongodb.net/article?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


let db = mongoose.connection;
db.on('error',console.error.bind(console, "Connection Error"));
db.once('open',() => console.log("Connected to MongoDB"));

app.use(express.json());

const articleRoutes = require('./routes/articleRoutes')

app.use('/articles', articleRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));