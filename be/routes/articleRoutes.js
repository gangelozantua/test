const express = require('express');
const router = express.Router();

const articleControllers = require('../controllers/articleControllers')

router.post('/createArticle', articleControllers.createArticle)
router.get('/', articleControllers.getAllArticle)

router.get('/:id', articleControllers.getSingleArticle)

router.put('/:id', articleControllers.updateArticle)

router.delete('/:id', articleControllers.deleteArticle)

module.exports = router;